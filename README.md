# RADIO ASTRONOMIC SOFTWARE SPECTROMETER (SWSPEC)

(C) 2018 Jan Wagner / Guifre Molera
Licensed according to GNU GPL

## Contents:

* Introduction
* Compiling
* Implementation details
* Input and output formats
* Usage example

## 1 - Introduction

 SWSpec is a simple software spectrometer to be used along VLBI data.


## 2 - Compiling

#### Linux Installation

```
sudo apt-get install automake libtool
Install latest [IPP Libraries](https://software.seek.intel.com/performance-libraries) and choose to install into **/opt/intel**
Install [IPP 9.0 Legacy Libraries](https://software.intel.com/en-us/articles/intel-ipp-legacy-libraries) by copying contents of included macosx.zip into **/opt/intel/legacy** (1)
make 
cd gui
/compile_gui.sh
```

#### MacOS Installation (Additional steps from Linux Installation)

* brew install make
* brew install automake
* brew install libtool
* brew install fftw
* brew cask install xquartz
* brew tap nelsonnunes/homebrew-custom
* brew install plplot-x11
* brew install pyqt

## 3 - Implementation details

(1) see details:
Manually edit ippdefs90legacy.h as shown below.

    Before:
        #ifndef __IPPDEFS_90_LEGACY_H__
        #define __IPPDEFS_90_LEGACY_H__

        #include "ippbase.h"
        #include "ipptypes.h"

    After:
        #ifndef __IPPDEFS_90_LEGACY_H__
        #define __IPPDEFS_90_LEGACY_H__

        #include "ippbase.h"
        #include "ipptypes.h"
        #if !defined( __STDCALL )
        #define __STDCALL IPP_STDCALL
        #endif


## 4 - Input and output formats

  * Input data format supported: Mark5A/Mark5B/VDIF
  * Output data format supported: Text and binary files

## 5 - Usage example

  Short demo

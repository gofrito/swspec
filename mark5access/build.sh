case "$OSTYPE" in
  darwin*)  platform="osx" ;;
  *)        platform="other" ;;
esac

aclocal
if [[ $platform == 'osx' ]]; then
    glibtoolize --copy --force
else
    libtoolize --copy --force
fi
autoconf
autoheader
automake -a -c

./configure US_CPFLAGS="-03" --prefix=/usr/local
make clean
make
make install


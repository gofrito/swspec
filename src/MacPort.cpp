/************************************************************************
 * IA-32/x64 Software Spectrometer
 * Copyright (C) 2019 Nelson Nunes
 *
 * Changes required to support compiling on MacOS
 ************************************************************************/

#include "MacPort.h"
#include <cstddef>
#include <stdlib.h>

void *aligned_alloc(size_t alignment, size_t size)
{
    // alignment must be >= sizeof(void*)
    if(alignment < sizeof(void*))
    {
        alignment = sizeof(void*);
    }

    void *pointer;
    if(posix_memalign(&pointer, alignment, size) == 0)
        return pointer;

    return nullptr;
}
